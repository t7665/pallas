def gpu_fix(tf):
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
        import time
        # Sleep 50 ms as the prints are asynchronous
        time.sleep(0.05)

def no_gpu(_=None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

def none(_=None):
    pass
