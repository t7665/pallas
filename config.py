### FOLDERS ###

# Folder containing data files (train and eval samples, csv files, the final model)
data_folder = "../data"

# VGGish Weights
vggish_weights = f'{data_folder}/vggish/vggish_audioset_weights.h5'

# Input samples
sample_folder = f'{data_folder}/0_samples'

# Resampled samples folder
resampled_folder = f'{data_folder}/1_resampled'

# Combined samples folder
combined_folder = f'{data_folder}/2_augmented'

# Sample features folder
features_folder = f'{data_folder}/3_features'

# Model output folder
output_folder = f'{data_folder}/4_models'

### GLOBAL SETTINGS ###

# Set to false to disable GPU usage completely
use_gpu = True

# Enable if the GPU has a low amount of memory
fix_gpu = False

# Random seed
default_seed = 42

# Proportion of resampled files to be used for training
split_proportions = {'train': 0.8, 'test': 0.2}

### AUGMENTATION SETTINGS ###

# dB offsets to generate, used for combining samples
db_offsets = [-0, -6, -12]

# Whether to save combined samples or combine during feature extraction
save_combinations = False

# Number of sample combinations
max_combinations = 100

# The weight of the lower volume in a combination, as a function of the relative gain.
# Gain is <0, 1>, where 1 means the samples are the same volume and 0 means only the first sample is audible.
secondary_weight = lambda gain: 0.0

### MODEL SETTINGS ###

# Number of LSTM Cells
lstm_cells = 10

# Number of LSTM Layers
lstm_layers = 1

### LOCAL SETTINGS ###

try:
    from local_config import *
except ModuleNotFoundError:
    print('No local config file found, skipping')
    pass
except Exception as e:
    print('Could not load local config')
    raise(e)
