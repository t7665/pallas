import audioop
from scipy.io import wavfile
from scipy.signal import resample
import numpy as np

# Target sample rate
VGGISH_RATE = 16000
OPENL3_RATE = 48000

MONO = 1
STEREO = 2

def load_vggish_compatible(wavefile):
    """Loads the given wave file and returns the contents, converted to be vggish compatible, and its parameters.
    
    Parameters
    ----------
    wavefile : str
        The file to load
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    wave_sr, wave_frames = wavfile.read(wavefile)
    return ensure_vggish_compatible(wave_sr, wave_frames)

def load_openl3_compatible(wavefile):
    """Loads the given wave file and returns the contents, converted to be OpenL3 compatible, and its parameters.
    
    Parameters
    ----------
    wavefile : str
        The file to load
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    wave_sr, wave_frames = wavfile.read(wavefile)
    return ensure_openl3_compatible(wave_sr, wave_frames)

def ensure_vggish_compatible(sr, frames, left=1, right=1, target_rate=VGGISH_RATE):
    """Convert the frames to be vggish compatible
    
    Parameters
    ----------
    sample_rate: int
        The audio sample rate
    frames : numpy.ndarray
        An array representing an audio wave
    left : float, optional
        The proportion of the left channel when converting stereo to mono (default is 1)
    right : float, optional
        The proportion of the right channel when converting stereo to mono (default is 1)
    target_rate : int, optional
        The target framerate (default is the vggish compatible value 16KHz)
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    return ensure_rate(sr, ensure_mono(frames, left, right), target_rate)

def ensure_openl3_compatible(sr, frames, left=1, right=1, target_rate=48000):
    """Convert the frames to be OpenL3 compatible
    
    Parameters
    ----------
    sample_rate: int
        The audio sample rate
    frames : numpy.ndarray
        An array representing an audio wave
    left : float, optional
        The proportion of the left channel when converting stereo to mono (default is 1)
    right : float, optional
        The proportion of the right channel when converting stereo to mono (default is 1)
    target_rate : int, optional
        The target framerate (default is the OpenL3 compatible value 48KHz)
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    return ensure_rate(sr, ensure_mono(frames, left, right), target_rate)

def ensure_mono(frames, left=1, right=1):
    """Convert the frames to be mono, if stereo
    
    Parameters
    ----------
    frames : numpy.ndarray
        An array representing an audio wave
    left : float, optional
        The proportion of the left channel when converting stereo to mono (default is 1)
    right : float, optional
        The proportion of the right channel when converting stereo to mono (default is 1)
    
    Returns
    -------
    numpy.ndarray
        The audio wave frames
    """
    if frames.ndim != 1:
        return frames[:, 0] * left + frames[:, 1] * right
    return frames

def ensure_rate(sample_rate, frames, target_rate=VGGISH_RATE):
    """Convert the frames to be the given target_rate, if necessary
    
    Parameters
    ----------
    sample_rate : int
        The audio frame rate
    frames : numpy.ndarray
        An array representing an audio wave
    target_rate : int, optional
        The target framerate (default is the vggish compatible value 16KHz)
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    if sample_rate == target_rate:
        return sample_rate, frames
    frames_bytes = frames.tobytes()
    frames_resampled = audioop.ratecv(
        frames_bytes, 2, MONO, sample_rate, target_rate, None
    )[0]
    return target_rate, np.frombuffer(frames_resampled, dtype=np.int16)

def combine_files(in1, in2):
    """Combine the given input wave files and return the combined frames
    
    Parameters
    ----------
    in1 : str
        String pointing to a wave file
    in2 : str
        String pointing to a wave file
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    sr1, frames1 = load_vggish_compatible(in1)
    sr2, frames2 = load_vggish_compatible(in2)
    assert sr1 == sr2
    length = min(len(frames1), len(frames2))
    return sr1, frames1[:length] + frames2[:length]

def write_file(file, frames, frame_rate=VGGISH_RATE):
    """Write a wave file
    
    Parameters
    ----------
    file : str
        The destination wave file
    frames : numpy.ndarray
        An array representing an audio wave
    frame_rate : int, optional
        The framerate of the target file (default is the vggish rate, 16 KHz)
    """
    assert frames.dtype == np.int16
    wavfile.write(file, frame_rate, frames)

def add_noise(frames, snr, noise_type='white'):
    """
    Parameters
    ----------
    frames : numpy.ndarray
        An array representing an audio wave
    snr : float
        The snr of the result signal.
    noise_type : str, optional
        The type of noise to add. Default is white noise ("white").
        Currently only accepts "white"
    
    Returns
    -------
    numpy.ndarray
        The audio wave frames
    """
    float_frames = frames / 2**16
    frames_power = float_frames ** 2

    # Calculate signal power and convert to dB
    mean_power = np.mean(frames_power)
    mean_db = 10 * np.log10(mean_power)
    # Calculate noise dB then convert to power
    noise_mean_db = mean_db - snr
    noise_mean_power = 10 ** (noise_mean_db / 10)
    # Generate the noise
    noise_mean = 0
    noise = np.random.normal(noise_mean, np.sqrt(noise_mean_power), float_frames.shape)
    # Noise up the original signal
    result = frames + (noise * 2**16).astype(np.int16)
    return result
