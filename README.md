# Pallas

This repository contains the machine learning code for the Pallas network, as a part of my Master's Thesis. It is loosely based on [Serval](https://github.com/SensingClues/serval).

## Installation & usage

- Clone the repository
- Update config.py according to your wishes
    - Optionally, create a file called "local_config.py" to override configurations found in config.py
- For each label, place your samples in a folder called "sample_folder/[label]" in config.py
- Now you can run 0-stack.ipynb
    - If you run into CUDA errors either simply rerun, or try with "fix_gpu" enabled or "use_gpu" disabled.
