{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "DwYXjpdRnYhV"
   },
   "source": [
    "# Custom metric\n",
    "\n",
    "Keras makes working with neural networks, especially DNNs, very easy. The reason for this is the high level API. One of the things one can do is evaluate the learning process on custom metrics by extending the class `tf.keras.metrics.Metric`. In this notebook we will look at a custom metric that computes the confusion matrix and is capable of giving the recall, precision and f1 states.\n",
    "\n",
    "While the ability to make custom metrics has been there for some time, tensorflow 2.2 has introduced model methods `train_step`, `test_step` and `predict_step` that are called by `model.fit` and `model.evaluate` etc. on a single batch. Now it is possible to call the `model.fit` method even for custom train steps (earlier this was done for the subclassing API as unbound functions that were not called by `model.fit`)\n",
    "\n",
    "\n",
    "We will try this out on the Fashion MNIST dataset. We begin by standard imports."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "1glZlw5mnYhZ"
   },
   "source": [
    "<table align=\"left\">\n",
    "  <td>\n",
    "    <a target=\"_blank\" href=\"https://colab.research.google.com/github/borundev/ml_cookbook/blob/master/Custom%20Metric%20(Confusion%20Matrix)%20and%20train_step%20method.ipynb\"><img src=\"https://www.tensorflow.org/images/colab_logo_32px.png\" />Run in Google Colab</a>\n",
    "  </td>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow import keras\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "bbuk9d1unYhc"
   },
   "source": [
    "Next we extend `tf.keras.metrics.Metric` to make a confusion matrix based metric. This object takes in true values and predictions (as probabilities) and updates a confusion matrix is maintains over an epoch. Internally, the fit method of the model calls this objects `update_state`. At the beginning of an epoch its method.\n",
    "\n",
    "This object has to implement `reset_states` is called and in this case it sets all values to 0.\n",
    "\n",
    "This object's `result` method returns the precision, recall and f1.\n",
    "\n",
    "Finally, we have a method `fill_output` that takes in the output dict and updates its values. This dict is used by the model's `train_step` to fill in the results of training on a batch.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "5rr4PvpunYhd"
   },
   "outputs": [],
   "source": [
    "class ConfusionMatrixMetric(tf.keras.metrics.Metric):\n",
    "    \"\"\"\n",
    "    A custom Keras metric to compute the running average of the confusion matrix\n",
    "    \"\"\"\n",
    "    def __init__(self, num_classes, **kwargs):\n",
    "        super(ConfusionMatrixMetric,self).__init__(name='confusion_matrix_metric',**kwargs) # handles base args (e.g., dtype)\n",
    "        self.num_classes=num_classes\n",
    "        self.total_cm = self.add_weight(\"total\", shape=(num_classes,num_classes), initializer=\"zeros\")\n",
    "        \n",
    "    def reset_state(self):\n",
    "        for s in self.variables:\n",
    "            s.assign(tf.zeros(shape=s.shape))\n",
    "            \n",
    "    def update_state(self, y_true, y_pred,sample_weight=None):\n",
    "        self.total_cm.assign_add(self.confusion_matrix(y_true,y_pred))\n",
    "        return self.total_cm\n",
    "        \n",
    "    def result(self):\n",
    "        return self.process_confusion_matrix()\n",
    "    \n",
    "    def confusion_matrix(self,y_true, y_pred):\n",
    "        \"\"\"\n",
    "        Make a confusion matrix\n",
    "        \"\"\"\n",
    "        y_true=tf.argmax(y_true,1)\n",
    "        y_pred=tf.argmax(y_pred,1)\n",
    "        cm=tf.math.confusion_matrix(y_true,y_pred,dtype=tf.float32,num_classes=self.num_classes)\n",
    "        return cm\n",
    "    \n",
    "    def process_confusion_matrix(self):\n",
    "        \"returns precision, recall and f1 along with overall accuracy\"\n",
    "        cm=self.total_cm\n",
    "        diag_part=tf.linalg.diag_part(cm)\n",
    "        precision=diag_part/(tf.reduce_sum(cm,0)+tf.constant(1e-15))\n",
    "        recall=diag_part/(tf.reduce_sum(cm,1)+tf.constant(1e-15))\n",
    "        f1=2*precision*recall/(precision+recall+tf.constant(1e-15))\n",
    "        return precision,recall,f1\n",
    "    \n",
    "    def fill_output(self,output):\n",
    "        results=self.result()\n",
    "        for i in range(self.num_classes):\n",
    "            output['precision_{}'.format(i)]=results[0][i]\n",
    "            output['recall_{}'.format(i)]=results[1][i]\n",
    "            output['F1_{}'.format(i)]=results[2][i]\n",
    "        output['precision_mean']=tf.math.reduce_mean(results[0])\n",
    "        output['recall_mean']=tf.math.reduce_mean(results[1])\n",
    "        output['F1_mean']=tf.math.reduce_mean(results[2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "L7y-qACCnYhe"
   },
   "source": [
    "Next we override the `keras.Sequential` class to add the methods `train_step` and `test_step` so that the custom behavior required to handle the confusion matrix metric can be handled (this is because the custom metric does not return a single value but precisions, recalls and f1s for all classes)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "nbvOK7QwnYhf"
   },
   "outputs": [],
   "source": [
    "class MySequential(keras.Sequential):\n",
    "    \n",
    "    def train_step(self, data):\n",
    "        # Unpack the data. Its structure depends on your model and\n",
    "        # on what you pass to `fit()`.\n",
    "\n",
    "        x, y = data\n",
    "\n",
    "        with tf.GradientTape() as tape:\n",
    "            y_pred = self(x, training=True)  # Forward pass\n",
    "            # Compute the loss value.\n",
    "            # The loss function is configured in `compile()`.\n",
    "            loss = self.compiled_loss(\n",
    "                y,\n",
    "                y_pred,\n",
    "                regularization_losses=self.losses,\n",
    "            )\n",
    "\n",
    "        # Compute gradients\n",
    "        trainable_vars = self.trainable_variables\n",
    "        gradients = tape.gradient(loss, trainable_vars)\n",
    "\n",
    "        # Update weights\n",
    "        self.optimizer.apply_gradients(zip(gradients, trainable_vars))\n",
    "        self.compiled_metrics.update_state(y, y_pred)\n",
    "        output={m.name: m.result() for m in self.metrics[:-1]}\n",
    "        if 'confusion_matrix_metric' in self.metrics_names:\n",
    "            self.metrics[-1].fill_output(output)\n",
    "        return output\n",
    "        \n",
    "        \n",
    "    def test_step(self, data):\n",
    "        # Unpack the data. Its structure depends on your model and\n",
    "        # on what you pass to `fit()`.\n",
    "\n",
    "        x, y = data\n",
    "\n",
    "        y_pred = self(x, training=False)  # Forward pass\n",
    "        # Compute the loss value.\n",
    "        # The loss function is configured in `compile()`.\n",
    "        loss = self.compiled_loss(\n",
    "            y,\n",
    "            y_pred,\n",
    "            regularization_losses=self.losses,\n",
    "        )\n",
    "\n",
    "        self.compiled_metrics.update_state(y, y_pred)\n",
    "        output={m.name: m.result() for m in self.metrics[:-1]}\n",
    "        if 'confusion_matrix_metric' in self.metrics_names:\n",
    "            self.metrics[-1].fill_output(output)    \n",
    "        return output\n",
    "            \n"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "name": "Custom Metric (Confusion Matrix) and train_step method.ipynb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
